use bevy::diagnostic::FrameTimeDiagnosticsPlugin;
use bevy::prelude::*;
use bevy::window::{PresentMode, WindowTheme};
use bevy_flycam::prelude::*;

mod engine;

fn axes_debug(mut gizmos: Gizmos) {
  gizmos.arrow(Vec3::ZERO, Vec3::X, Color::GREEN);
  gizmos.arrow(Vec3::ZERO, Vec3::Y, Color::RED);
  gizmos.arrow(Vec3::ZERO, Vec3::Z, Color::BLUE);
}

fn main() {
  App::new()
    .add_plugins((
      DefaultPlugins.set(WindowPlugin {
        primary_window: Some(Window {
          title: "welt".into(),
          name: Some("welt".into()),
          //resolution: (500., 300.).into(),
          present_mode: PresentMode::Immediate,
          window_theme: Some(WindowTheme::Dark),
          enabled_buttons: bevy::window::EnabledButtons {
            maximize: false,
            ..Default::default()
          },
          ..default()
        }),
        ..default()
      }),
      MaterialPlugin::<engine::voxel::CustomMaterial>::default(),
    ))
    .add_plugins(FrameTimeDiagnosticsPlugin)
    .add_plugins(PlayerPlugin)
    //.add_systems(Update, axes_debug)
    .add_systems(
      Startup,
      (
        setup,
        engine::world::setup,
        engine::world::setup_neighbours,
        engine::chunk::build_mesh,
      )
        .chain(),
    )
    .add_systems(Startup, engine::fps::setup_fps_counter)
    .add_systems(
      Update,
      (
        engine::fps::fps_text_update_system,
        engine::fps::fps_counter_showhide,
      ),
    )
    .run();
}

/// set up a simple 3D scene
fn setup(mut commands: Commands) -> () {
  commands.spawn(engine::world::World::new());
  commands.spawn(engine::procgen::ProcGen::new());

  // light
  commands.spawn(PointLightBundle {
    point_light: PointLight {
      shadows_enabled: true,
      ..default()
    },
    transform: Transform::from_xyz(4.0, 8.0, 4.0),
    ..default()
  });
}
