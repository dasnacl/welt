use bevy::math::u16::u16vec3;
use bevy::prelude::*;

use noise::{NoiseFn, Perlin};

use super::voxel::{Block, BlockType};

#[derive(Component)]
pub struct ProcGen {
  perlin: Perlin,
}

impl ProcGen {
  pub fn new() -> ProcGen {
    ProcGen {
      perlin: Perlin::new(1),
    }
  }

  pub fn get(&mut self, pos: IVec3) -> Block {
    let val = self
      .perlin
      .get([pos.x as f64 + 0.5, pos.y as f64 + 0.5, pos.z as f64 + 0.5]);
    let ty = if val > -1.0 {
      BlockType::Solid
    } else {
      BlockType::Air
    };
    Block::new(u16vec3(pos.x as u16, pos.y as u16, pos.z as u16), ty)
  }
}
