use bevy::math::i32::ivec3;
use bevy::prelude::*;
use bevy::render::{
  mesh::Indices, render_asset::RenderAssetUsages, render_resource::PrimitiveTopology,
};

use super::voxel::Block;
use super::world::ActiveChunks;

pub const CHUNK_SIZE: u32 = 16;
pub const CHUNK_SIZE_I: i32 = 16;
pub const CHUNK_SIZE_S: usize = CHUNK_SIZE as usize;

#[derive(Debug, Clone)]
pub struct Local(pub IVec3);
#[derive(Debug, Clone)]
pub struct Global(pub IVec3);

fn li3(x: i32, y: i32, z: i32) -> Local {
  Local(IVec3 { x, y, z })
}

#[derive(Component)]
pub struct Chunk {
  pos: IVec3,
  neighbours: [Option<Entity>; 6],
  blocks: Vec<Block>,
}

impl Chunk {
  pub fn new(pos: IVec3, neighbours: [Option<Entity>; 6], blocks: Vec<Block>) -> Chunk {
    Chunk {
      pos,
      neighbours,
      blocks,
    }
  }
  pub const fn toidx(x: i32, y: i32, z: i32) -> usize {
    (x * CHUNK_SIZE_I * CHUNK_SIZE_I + y * CHUNK_SIZE_I + z) as usize
  }
  pub const fn fromidx(idx: usize) -> (i32, i32, i32) {
    let z = idx / (CHUNK_SIZE_S * CHUNK_SIZE_S);
    let idx = idx - z * CHUNK_SIZE_S * CHUNK_SIZE_S;

    let y = idx / CHUNK_SIZE_S;
    let x = idx - y * CHUNK_SIZE_S;

    (x as i32, y as i32, z as i32)
  }
  pub const fn enforce_boundary(v: IVec3) -> IVec3 {
    let mut x = v.x;
    while x < 0 {
      x += CHUNK_SIZE_I;
    }
    while x >= CHUNK_SIZE_I {
      x -= CHUNK_SIZE_I;
    }
    let mut y = v.y;
    while y < 0 {
      y += CHUNK_SIZE_I;
    }
    while y >= CHUNK_SIZE_I {
      y -= CHUNK_SIZE_I;
    }
    let mut z = v.z;
    while z < 0 {
      z += CHUNK_SIZE_I;
    }
    while z >= CHUNK_SIZE_I {
      z -= CHUNK_SIZE_I;
    }
    IVec3::new(x, y, z)
  }
  pub const fn max_size() -> usize {
    CHUNK_SIZE_S * CHUNK_SIZE_S * CHUNK_SIZE_S
  }
  pub fn set_neighbours(&mut self, v: [Option<Entity>; 6]) -> () {
    self.neighbours = v;
  }
  pub fn all_iter(base: IVec3) -> impl Iterator<Item = (i32, i32, i32)> {
    (0..CHUNK_SIZE_I).flat_map(move |i| {
      (0..CHUNK_SIZE_I)
        .flat_map(move |j| (0..CHUNK_SIZE_I).map(move |k| (base.x + k, base.y + j, base.z + i)))
    })
  }
  pub fn get_block(&self, x: i32, y: i32, z: i32) -> Block {
    self.blocks[Self::toidx(x, y, z)]
  }
  pub fn is_air(&self, l: Local) -> bool {
    let Local(IVec3 { x, y, z }) = l;
    if x < 0 || y < 0 || z < 0 || x >= CHUNK_SIZE_I || y >= CHUNK_SIZE_I || z >= CHUNK_SIZE_I {
      return true;
    }
    self.get_block(x, y, z).is_air()
  }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Axis {
  X,
  Y,
  Z,
}
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct Side(pub Axis, pub bool);

impl Side {
  fn inorm(&self) -> [i32; 3] {
    let sign = if self.1 { 1 } else { -1 } as i32;
    match self.0 {
      Axis::X => [sign, 0, 0],
      Axis::Y => [0, sign, 0],
      Axis::Z => [0, 0, sign],
    }
  }
  fn norm(&self) -> [f32; 3] {
    let sign = if self.1 { 1 } else { -1 } as f32;
    match self.0 {
      Axis::X => [sign, 0.0, 0.0],
      Axis::Y => [0.0, sign, 0.0],
      Axis::Z => [0.0, 0.0, sign],
    }
  }
  fn norms(&self) -> [[f32; 3]; 4] {
    let n = self.norm();
    [n, n, n, n]
  }
  fn is_border(&self, v: IVec3, min: i32, max: i32) -> bool {
    match self.0 {
      Axis::X => {
        if !self.1 {
          v.x == min
        } else {
          v.x == max - 1
        }
      }
      Axis::Y => {
        if !self.1 {
          v.y == min
        } else {
          v.y == max - 1
        }
      }
      Axis::Z => {
        if !self.1 {
          v.z == min
        } else {
          v.z == max - 1
        }
      }
    }
  }
  fn neighbour(&self, v: IVec3) -> IVec3 {
    let sign = if self.1 { 1 } else { -1 };
    match self.0 {
      Axis::X => IVec3::new(v.x + sign, v.y, v.z),
      Axis::Y => IVec3::new(v.x, v.y + sign, v.z),
      Axis::Z => IVec3::new(v.x, v.y, v.z + sign),
    }
  }
}
impl Into<usize> for Side {
  fn into(self) -> usize {
    match self {
      Side(Axis::X, false) => 0, // X-
      Side(Axis::X, true) => 1,  // X+
      Side(Axis::Y, false) => 2, // Y-
      Side(Axis::Y, true) => 3,  // Y+
      Side(Axis::Z, false) => 4, // Z-
      Side(Axis::Z, true) => 5,  // Z+
    }
  }
}
impl From<usize> for Side {
  fn from(value: usize) -> Self {
    match value {
      0 => Side(Axis::X, false), // X-
      1 => Side(Axis::X, true),  // X+
      2 => Side(Axis::Y, false), // Y-
      3 => Side(Axis::Y, true),  // Y+
      4 => Side(Axis::Z, false), // Z-
      5 => Side(Axis::Z, true),  // Z+
      _ => unreachable!("Could not convert side index to an orthonormal."),
    }
  }
}
#[derive(Debug)]
struct Face {
  side: Side,
  points: [f32; 3],
}
impl Face {
  fn indices(&self, offset: u32) -> [u32; 6] {
    [
      offset + 0,
      offset + 2,
      offset + 1,
      offset + 1,
      offset + 2,
      offset + 3,
    ]
  }
  fn vertices(&self, scale: f32) -> [[f32; 3]; 4] {
    let (x, y, z) = (self.points[0], self.points[1], self.points[2]);

    let verts = match self.side {
      Side(Axis::X, false) => [
        [0.0, 0.0, 1.0],
        [0.0, 0.0, 0.0],
        [0.0, 1.0, 1.0],
        [0.0, 1.0, 0.0],
      ],
      Side(Axis::X, true) => [
        [1.0, 0.0, 0.0],
        [1.0, 0.0, 1.0],
        [1.0, 1.0, 0.0],
        [1.0, 1.0, 1.0],
      ],
      Side(Axis::Y, false) => [
        [0.0, 0.0, 1.0],
        [1.0, 0.0, 1.0],
        [0.0, 0.0, 0.0],
        [1.0, 0.0, 0.0],
      ],
      Side(Axis::Y, true) => [
        [0.0, 1.0, 1.0],
        [0.0, 1.0, 0.0],
        [1.0, 1.0, 1.0],
        [1.0, 1.0, 0.0],
      ],
      Side(Axis::Z, false) => [
        [0.0, 0.0, 0.0],
        [1.0, 0.0, 0.0],
        [0.0, 1.0, 0.0],
        [1.0, 1.0, 0.0],
      ],
      Side(Axis::Z, true) => [
        [1.0, 0.0, 1.0],
        [0.0, 0.0, 1.0],
        [1.0, 1.0, 1.0],
        [0.0, 1.0, 1.0],
      ],
    };
    [
      [
        x * scale + verts[0][0] * scale,
        y * scale + verts[0][1] * scale,
        z * scale + verts[0][2] * scale,
      ],
      [
        x * scale + verts[1][0] * scale,
        y * scale + verts[1][1] * scale,
        z * scale + verts[1][2] * scale,
      ],
      [
        x * scale + verts[2][0] * scale,
        y * scale + verts[2][1] * scale,
        z * scale + verts[2][2] * scale,
      ],
      [
        x * scale + verts[3][0] * scale,
        y * scale + verts[3][1] * scale,
        z * scale + verts[3][2] * scale,
      ],
    ]
  }
  fn normals(&self) -> [[f32; 3]; 4] {
    self.side.norms()
  }
  fn uvs(&self, pos_u: bool, pos_v: bool) -> [[f32; 2]; 4] {
    match (pos_u, pos_v) {
      (true, true) => [[1.0, 1.0], [0.0, 1.0], [1.0, 0.0], [0.0, 0.0]],
      (true, false) => [[1.0, 0.0], [0.0, 0.0], [1.0, 1.0], [0.0, 1.0]],
      (false, true) => [[0.0, 1.0], [1.0, 1.0], [0.0, 0.0], [1.0, 0.0]],
      (false, false) => [[0.0, 0.0], [1.0, 0.0], [0.0, 1.0], [1.0, 1.0]],
    }
  }
}
// Wrapper type to build iterator
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
struct V(IVec3);
impl V {
  const SIDES: [Side; 6] = [
    Side(Axis::X, false),
    Side(Axis::X, true),
    Side(Axis::Y, false),
    Side(Axis::Y, true),
    Side(Axis::Z, false),
    Side(Axis::Z, true),
  ];
  fn neighbours(self, chunk: &Chunk) -> impl Iterator<Item = (Side, bool)> + '_ {
    (0..6).map(move |i| {
      let n = Self::SIDES[i].inorm();
      (
        Self::SIDES[i],
        chunk.is_air(Local(ivec3(
          n[0] + self.0.x,
          n[1] + self.0.y,
          n[2] + self.0.z,
        ))),
      )
    })
  }
}

pub fn build_mesh(
  mut cmds: Commands,
  mut meshes: ResMut<Assets<Mesh>>,
  mut materials: ResMut<Assets<StandardMaterial>>,
  chunks: Query<&mut Chunk>,
) {
  let mut mchunks: Vec<_> = chunks.into_iter().collect();
  for chunk in mchunks.iter_mut() {
    let mut verts = vec![];
    let mut norms: Vec<[f32; 3]> = vec![];
    let mut uvs: Vec<[f32; 2]> = vec![];
    let mut indices: Vec<u32> = vec![];

    for (x, y, z) in Chunk::all_iter(ivec3(0, 0, 0)) {
      if chunk.is_air(li3(x, y, z)) {
        continue;
      }
      for (side, nis_air) in V(ivec3(x, y, z)).neighbours(&chunk) {
        let side_idx: usize = side.into();
        let bordercon = if side.is_border(ivec3(x, y, z), 0, CHUNK_SIZE_I) {
          let cn = chunk.neighbours[side_idx];
          if let Some(cn) = cn {
            chunks
              .get(cn)
              .expect("chunk neighbours setup incorrectly")
              .is_air(Local(Chunk::enforce_boundary(
                side.neighbour(ivec3(x, y, z)),
              )))
          } else {
            true
          }
        } else {
          true
        };
        // it's only air if within this chunk and outside of this chunk it isn't air (boundary condition)
        if nis_air && bordercon {
          let f = Face {
            side,
            points: [x as f32, y as f32, z as f32],
          };
          let ln = verts.len();
          verts.extend_from_slice(&f.vertices(1.0));
          norms.extend_from_slice(&f.normals());
          uvs.extend_from_slice(&f.uvs(false, true));
          indices.extend_from_slice(&f.indices(ln as u32));
        }
      }
    }
    let handle = meshes.add(
      Mesh::new(
        PrimitiveTopology::TriangleList,
        RenderAssetUsages::MAIN_WORLD | RenderAssetUsages::RENDER_WORLD,
      )
      .with_inserted_attribute(
        Mesh::ATTRIBUTE_POSITION,
        verts
          .into_iter()
          .map(|x| [x[0] as f32, x[1] as f32, x[2] as f32])
          .collect::<Vec<[f32; 3]>>(),
      )
      .with_inserted_attribute(Mesh::ATTRIBUTE_UV_0, uvs)
      .with_inserted_attribute(Mesh::ATTRIBUTE_NORMAL, norms)
      .with_inserted_indices(Indices::U32(indices)),
    );
    cmds.spawn(PbrBundle {
      mesh: handle,
      material: materials.add(Color::PURPLE),
      transform: Transform::from_xyz(chunk.pos.x as f32, chunk.pos.y as f32, chunk.pos.z as f32),
      ..default()
    });
  }
}
