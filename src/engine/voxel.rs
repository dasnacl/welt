use bevy::math::u16::{u16vec3, U16Vec3};
use bevy::prelude::*;
use bevy::render::render_resource::AsBindGroup;

#[derive(Asset, TypePath, AsBindGroup, Clone)]
pub struct CustomMaterial {
  //#[uniform(0)]
  //color: Color,
}

impl Material for CustomMaterial {
  //fn fragment_shader() -> ShaderRef {
  //  "shaders/shader_defs.wgsl".into()
  //}
  //fn specialize(
  //  _pipeline: &MaterialPipeline<Self,>,
  //  descriptor: &mut RenderPipelineDescriptor,
  //  _layout: &MeshVertexBufferLayout,
  //  key: MaterialPipelineKey<Self,>,
  //) -> Result<(), SpecializedMeshPipelineError,> {
  //  //  if key.bind_group_data.is_red {
  //  //    let fragment = descriptor.fragment.as_mut().unwrap();
  //  //    fragment.shader_defs.push("IS_RED".into(),);
  //  //  }
  //  Ok((),)
  //}
}

type BlockBaseT = u16;
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum BlockType {
  Air = 0,
  Solid,
}
impl BlockType {
  // This has to be a const fn
  pub const fn into_bits(self) -> BlockBaseT {
    self as _
  }
  pub fn from_bits(value: BlockBaseT) -> Self {
    match value {
      0 => Self::Air,
      1 => Self::Solid,
      _ => Self::Air,
    }
  }
}

#[derive(Component, Debug, Clone, Copy, PartialEq, Eq)]
pub struct Block(BlockBaseT);

const X_MASK: BlockBaseT = 0b1111000000000000;
const Y_MASK: BlockBaseT = 0b0000111100000000;
const Z_MASK: BlockBaseT = 0b0000000011110000;
const MATERIAL_MASK: BlockBaseT = 0b0000000000001111;

const X_SHIFT: BlockBaseT = 12;
const Y_SHIFT: BlockBaseT = 8;
const Z_SHIFT: BlockBaseT = 4;
const MATERIAL_SHIFT: BlockBaseT = 0;

impl Block {
  pub fn is_air(&self) -> bool {
    self.ty() == BlockType::Air
  }
  pub fn set_type(&mut self, ty: BlockType) -> BlockType {
    let b = ty.into_bits();
    self.0 = (self.0 & !MATERIAL_MASK) | (b << MATERIAL_SHIFT);
    ty
  }
  pub fn ty(&self) -> BlockType {
    BlockType::from_bits((self.0 & MATERIAL_MASK) >> MATERIAL_SHIFT)
  }
  pub fn x(&self) -> BlockBaseT {
    (self.0 & X_MASK) >> X_SHIFT
  }
  pub fn y(&self) -> BlockBaseT {
    (self.0 & Y_MASK) >> Y_SHIFT
  }
  pub fn z(&self) -> BlockBaseT {
    (self.0 & Z_MASK) >> Z_SHIFT
  }
  pub fn xyz(&self) -> U16Vec3 {
    u16vec3(self.x(), self.y(), self.z())
  }

  pub const fn new(xyz: U16Vec3, ty: BlockType) -> Self {
    let dat: BlockBaseT = 0;
    let dat = dat | (xyz.x & 0b1111) << X_SHIFT;
    let dat = dat | (xyz.y & 0b1111) << Y_SHIFT;
    let dat = dat | (xyz.z & 0b1111) << Z_SHIFT;
    let dat = dat | (ty.into_bits() & 0b1111) << MATERIAL_SHIFT;
    Block(dat)
  }
}
