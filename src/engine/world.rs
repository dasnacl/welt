use bevy::math::i32::ivec3;
use bevy::prelude::*;

use super::chunk::{Axis, Chunk, Side, CHUNK_SIZE_I};
use super::procgen::ProcGen;

const WORLD_SIZE_W: i32 = 2;
const WORLD_SIZE_H: i32 = 2;
const WORLD_SIZE_D: i32 = 2;

const WORLD_AREA: i32 = WORLD_SIZE_W * WORLD_SIZE_D;
const WORLD_VOLUME: i32 = WORLD_AREA * WORLD_SIZE_H;

const WORLD_CENTER_XZ: i32 = WORLD_SIZE_W * CHUNK_SIZE_I;
const WORLD_CENTER_Y: i32 = WORLD_SIZE_H * CHUNK_SIZE_I;

#[derive(Debug, Clone)]
pub struct Local {
  x: i32,
  y: i32,
  z: i32,
}
#[derive(Debug, Clone)]
pub struct Global {
  x: i32,
  y: i32,
  z: i32,
}

fn li3(x: i32, y: i32, z: i32) -> Local {
  Local { x, y, z }
}
fn gi3(x: i32, y: i32, z: i32) -> Global {
  Global { x, y, z }
}
impl Into<IVec3> for Global {
  fn into(self) -> IVec3 {
    IVec3::new(self.x, self.y, self.z)
  }
}
impl Into<IVec3> for Local {
  fn into(self) -> IVec3 {
    IVec3::new(self.x, self.y, self.z)
  }
}
impl From<Global> for Local {
  fn from(v: Global) -> Self {
    li3(v.x / CHUNK_SIZE_I, v.y / CHUNK_SIZE_I, v.z / CHUNK_SIZE_I)
  }
}
impl From<Local> for Global {
  fn from(v: Local) -> Self {
    gi3(v.x * CHUNK_SIZE_I, v.y * CHUNK_SIZE_I, v.z * CHUNK_SIZE_I)
  }
}

#[derive(Component)]
pub struct NeighbouringInformation {
  data: Vec<[Option<Entity>; 6]>,
}
#[derive(Component)]
pub struct ActiveChunks {
  pub chunks: Vec<Entity>,
}

#[derive(Component)]
pub struct World;

pub fn spawn_chunk(procgen: &mut ProcGen, pos: IVec3) -> Chunk {
  let mut v = vec![];
  for (x, y, z) in Chunk::all_iter(pos) {
    v.push(procgen.get(ivec3(x, y, z)));
  }
  let neighbours: [Option<Entity>; 6] = [None, None, None, None, None, None];
  Chunk::new(pos, neighbours, v)
}

impl World {
  pub const fn toidx(local: Local) -> usize {
    let x = local.x;
    let y = local.y;
    let z = local.z;
    (x * WORLD_SIZE_W * WORLD_SIZE_H + y * WORLD_SIZE_H + z) as usize
  }
  pub const fn fromidx(idx: i32) -> Local {
    let z = idx / (WORLD_SIZE_W * WORLD_SIZE_H);
    let idx = idx - z * WORLD_SIZE_H * WORLD_SIZE_H;

    let y = idx / WORLD_SIZE_H;
    let x = idx - y * WORLD_SIZE_H;

    Local {
      x: x as i32,
      y: y as i32,
      z: z as i32,
    }
  }
  pub fn all_iter() -> impl Iterator<Item = Local> {
    (0..WORLD_SIZE_D).flat_map(move |i| {
      (0..WORLD_SIZE_H).flat_map(move |j| (0..WORLD_SIZE_W).map(move |k| li3(k, j, i)))
    })
  }

  pub fn new() -> World {
    World {}
  }
  pub fn build_chunks(&mut self, cmds: &mut Commands, procgen: &mut ProcGen) -> () {
    let mut neighbors = vec![];
    neighbors.resize_with(WORLD_VOLUME as usize, || None);
    for l in Self::all_iter() {
      let global: Global = l.clone().into();
      let id = cmds.spawn(spawn_chunk(procgen, global.into())).id();
      neighbors[Self::toidx(l)] = Some(id);
    }
    let mut actual_neighbours: Vec<[Option<Entity>; 6]> = vec![];
    actual_neighbours.resize_with(WORLD_VOLUME as usize, || {
      [None, None, None, None, None, None]
    });
    // setup neighbours
    for l in Self::all_iter() {
      let Local { x, y, z } = l.clone();
      let idx = Self::toidx(li3(x, y, z));

      // setup neighbours
      if x - 1 >= 0 {
        let at: usize = Side(Axis::X, false).into();
        let coat: usize = Side(Axis::X, true).into();

        let widx = Self::toidx(li3(x - 1, y, z));

        actual_neighbours[idx][at] = neighbors[widx];
        actual_neighbours[widx][coat] = neighbors[idx];
      }
      if y - 1 >= 0 {
        let at: usize = Side(Axis::Y, false).into();
        let coat: usize = Side(Axis::Y, true).into();

        let widx = Self::toidx(li3(x, y - 1, z));
        actual_neighbours[idx][at] = neighbors[widx];
        actual_neighbours[widx][coat] = neighbors[idx];
      }
      if z - 1 >= 0 {
        let at: usize = Side(Axis::Z, false).into();
        let coat: usize = Side(Axis::Z, true).into();

        let widx = Self::toidx(li3(x, y, z - 1));
        actual_neighbours[idx][at] = neighbors[widx];
        actual_neighbours[widx][coat] = neighbors[idx];
      }
      if x + 1 < WORLD_SIZE_W {
        let at: usize = Side(Axis::X, true).into();
        let coat: usize = Side(Axis::X, false).into();

        let widx = Self::toidx(li3(x + 1, y, z));
        actual_neighbours[idx][at] = neighbors[widx];
        actual_neighbours[widx][coat] = neighbors[idx];
      }
      if y + 1 < WORLD_SIZE_H {
        let at: usize = Side(Axis::Y, true).into();
        let coat: usize = Side(Axis::Y, false).into();

        let widx = Self::toidx(li3(x, y + 1, z));
        actual_neighbours[idx][at] = neighbors[widx];
        actual_neighbours[widx][coat] = neighbors[idx];
      }
      if z + 1 < WORLD_SIZE_D {
        let at: usize = Side(Axis::Z, true).into();
        let coat: usize = Side(Axis::Z, false).into();

        let widx = Self::toidx(li3(x, y, z + 1));
        actual_neighbours[idx][at] = neighbors[widx];
        actual_neighbours[widx][coat] = neighbors[idx];
      }
    }
    cmds.spawn(NeighbouringInformation {
      data: actual_neighbours,
    });
    cmds.spawn(ActiveChunks {
      chunks: neighbors.into_iter().flatten().collect(),
    });
  }
}
pub fn setup_neighbours(
  mut cmds: Commands,
  mut neighbours: Query<(Entity, &mut NeighbouringInformation)>,
  mut active_chunks: Query<&ActiveChunks>,
  mut chunks: Query<&mut Chunk>,
) -> () {
  let (del, neighbours) = neighbours.single_mut();
  let active = active_chunks.single_mut();
  for l in World::all_iter() {
    let idx = World::toidx(l);
    let mut ch = chunks
      .get_mut(active.chunks[idx])
      .expect("Chunk should have been visited");
    ch.set_neighbours(neighbours.data[idx]);
  }
  cmds.entity(del).despawn();
}

pub fn setup(
  mut cmds: Commands,
  mut world: Query<&mut World>,
  mut proc: Query<&mut ProcGen>,
) -> () {
  let (mut w, mut p) = (world.single_mut(), proc.single_mut());
  w.build_chunks(&mut cmds, &mut p);
}
