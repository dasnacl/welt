
# Welt


# Voxels

Voxels essentially store the following data:

```c
struct voxel_t {
    unsigned int x : 4;
    unsigned int y : 4;
    unsigned int z : 4;
    unsigned int material : 8;
}
```

Coordinates are relative to the chunk, so it suffices to be able to index 16 different values for each coordinate.



# notes

- [ ] do not mesh if not "visible"
- [ ] improve meshing
- [ ] textures
- [ ] world generation
- [ ] adjust camera to view one plot of land 
